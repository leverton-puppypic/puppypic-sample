# PuppyPic Sample Generator
This application populates some sample URLs for the PuppyPic application. This is a nodejs application.

# Usage
* Ensure that the database is running
* run the `generate.sh` script to populate the database
* The arguments for the application are as follows:
  * `--number {number}` specifies the number of links to populate (default: `100`)
  * `--clear` clears all the current images (default: `false`)
  * `--api {url}` provides the API URL for the PuppyPic application (default: `http://localhost:8080`)
* Example `./generate.sh --number 100 --api http://localhost:8080 --clear`

# Notes
* nodejs and npm need to be installed on the machine running this application
* Dependencies can be installed by using `npm install`