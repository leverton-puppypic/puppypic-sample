const argv = require('minimist')(process.argv.slice(2))
const puppy = require('random-puppy')
const async = require('async')
const q = require('q')
const axios = require('axios')
const _ = require('lodash')
const num = argv.number || 100
const api = argv.api || 'http://localhost:8080'

const range = _.range(0, num)
let count = 0
const clear = async () => {
  const def = q.defer()
  if (argv.clear) {
    const all = await axios.get(api).then(d => d.data)
    console.log(`Clearing ${all.length} images...`)
    async.eachLimit(all.map(a => a.id), 10, (id, nextid) => {
      axios.delete(`${api}/${id}`).then(() => { nextid() }).catch(nextid)
    }, err => {
      if (err) def.reject(err)
      else def.resolve()
      console.log(`Cleared ${all.length} images`)
    })
  } else def.resolve()
  return def.promise
}
const create = () => {
  count = 0
  async.eachLimit(range, 10, (n, next) => {
    puppy().then(url => {
      return axios.put(api, { url })
    }).then(() => {
      count += 1
      console.log(`Processed ${count} of ${num}`)
      next()
    }).catch(next)
  }, err => {
    if (err) console.error(err)
    else console.log('---------- Process complete ----------')
  })
}
clear().then(create).catch(console.error)
